function handleClick(type) {
  var navigation = document.querySelector(".navigation");
  if (type === "open") {
    navigation.style.display = "block";
  } else if (type == "close") {
    navigation.style.display = "none";
  }
}
